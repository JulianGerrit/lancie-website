# This Dockerfile is currenly made just for production. In the final image, you only have nginx serving the files.
# We use lts/stable alpine images in both stages because they're nice & small and reliable.

# This stage we use the node image to build the app. This gets put in /app/public
FROM node:lts-alpine AS build
WORKDIR /app

# Copy package.json & install dependencies first. This can be cahced
COPY package.json ./
RUN yarn install --prod

# Copy everything except what's in .dockerignore & build the application.
COPY . ./
RUN yarn build

# Fresh start where we only use the build we created + nginx to serve it.
# No node, no nodule_modules, just nginx and our build.
FROM nginx:stable-alpine AS final
WORKDIR /app

# nginx runs on port 80. See nginx/default.conf
EXPOSE 80

# Set up nginx to serve our build. Copy over the files and use our default.conf instead of the default default.conf.
COPY --from=build /app/public /usr/share/nginx/html/
COPY --from=build /app/nginx/default.conf /etc/nginx/conf.d/default.conf

# We do not specify a CMD here because we are just using the default nginx image CMD.

# Now the image can be run. Do make sure to configure host port x to container port 80.