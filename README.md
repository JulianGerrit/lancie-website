# LANcie website

A website for the LANcie committee of FMF, built with Typescript, Gatsby, and [NES.css](https://nostalgic-css.github.io/NES.css/) with a One-page layout.


## Features

Pretty limited for now (on purpose):

* One-page layout
* very basic SEO
* styled 404

For more customizations with the NES.css framework, check their nice documentation.

## Warning

This theme is only a nostalgic tribute to Nintendo. It's easy to customize it but it's pretty light in terms of features.

## Setup without docker
First, you'll need to install [NodeJS](https://nodejs.org/en/). This should install `npm` as well. Otherwise, install that.

We want to use `yarn` from now on, so we install that:
```
npm install -g yarn
```

### Install dependencies (do not use npm to do this)

```
yarn install
```

### 🌪 Starting the dev environment

```
yarn dev
```
The application will be on `localhost:`
### Weird issues

If you have some issues with static images and queries, you might save a lot of time by using the custom yarn command

```
yarn cleandev
``` 

### Compile for deployment
```
yarn build
```
This will compile your code to the `./public` folder. The `./public` folder can then be statically hosted like any other plain html/css/js website.

## Docker
This application also comes with a Dockerfile (mainly for production use). Simply use that and deploy easily, without needing to do anything else. This means that all of the above steps can be skipped.

One way of running the application using docker is with `docker compose up`. The application will then be on `localhost:8000`, just like the non-docker method.

If you're not using `docker compose`: Make sure to configure host port x to container port 80.

## Change Metadata

The important metadata, such as the title of the website can be found in `./gatsby-config.ts`

## humans.txt

We are humans, not machines. The humans.txt file is a tribute to the people who have contributed to the building of a website.
Go modify `./static/humans.txt` with your team info.

It will execute `gatsby clean` before `gatsby develop`, which deletes the cache folder and ensures there's no outdated stuff that can jam your app.

## About Preact

The theme uses [Preact](https://preactjs.com/), a much lighter alternative of React. Most gatsby websites and plugins will work fine, but some of them might be incompatible with it.

Be careful.

## 🎓 Learning Gatsby

Looking for more guidance? Full documentation for Gatsby lives [on the website](https://www.gatsbyjs.com/).

Here are some places to start:

### Themes

- To learn more about Gatsby themes specifically, we recommend checking out the [theme docs](https://www.gatsbyjs.com/docs/themes/).

### General

- **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.com/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.com/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.
