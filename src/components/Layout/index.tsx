import * as React from "react"

import "../../styles/nes.min.css"
import "../../styles/reset.css"
import "../../styles/styles.scss"
import { useEffect, useState } from "react"

type LayoutProps = {
  withHeader?: boolean
  title?: string
  description?: string
  author?: string
  children?: React.ReactNode
}

const gitlab = {
  text: `GitLab repo`,
  url: `https://gitlab.com/fmfnl/websites/lancie`,
  description: `Source code of this website`,
}

const Layout: React.FunctionComponent<LayoutProps> = ({ children, title, description, author, withHeader = true }) => {
  const [scrollPosition, setScrollPosition] = useState(0)
  const handleScroll = () => {
    const position = window.scrollY
    setScrollPosition(position)
  }

  useEffect(() => {
    window.addEventListener(`scroll`, handleScroll, { passive: true })

    return () => {
      window.removeEventListener(`scroll`, handleScroll)
    }
  }, [])

  return (
    <>
      {withHeader && (
        <header className={`header active ${scrollPosition > 30 && `hidden`}`}>
          <div className="inner">
            <div className="nav-brand">
              <span className="nes-text is-primary">
                <h1 id="title">
                  <i aria-hidden="true" className="snes-jp-logo brand-logo"></i> {title}
                </h1>
              </span>
              <p className="description nes-text is-disabled">{description}</p>
            </div>
          </div>
        </header>
      )}
      <main aria-labelledby="title"> {children}</main>
      <footer className="footer">
        &copy; {new Date().getFullYear()} {author} <br />
        <a href={gitlab.url}>{gitlab.description}</a>
      </footer>
    </>
  )
}

export default Layout
